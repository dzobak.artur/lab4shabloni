﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4Shabloni
{
    class Product
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public decimal Price { get; set; }

        public override string ToString()
        {
            return $"{Name} - {Manufacturer} - {Price:C}";
        }
    }

    class ShoppingCart
    {
        private List<Product> cartItems = new List<Product>();

        public void AddToCart(Product product)
        {
            cartItems.Add(product);
            Console.WriteLine($"Added {product} to the cart.");
        }

        public void RemoveFromCart(Product product)
        {
            if (cartItems.Contains(product))
            {
                cartItems.Remove(product);
                Console.WriteLine($"Removed {product} from the cart.");
            }
            else
            {
                Console.WriteLine($"{product} not found in the cart.");
            }
        }

        public void DisplayCart()
        {
            Console.WriteLine("Current Cart:");
            foreach (var item in cartItems)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }
    }

    interface ICommand<T>
    {
        void Execute(T item);
        void Undo(T item);
    }

    class AddToCartCommand : ICommand<Product>
    {
        private readonly ShoppingCart cart;
        private readonly Product product;

        public AddToCartCommand(ShoppingCart cart, Product product)
        {
            this.cart = cart;
            this.product = product;
        }

        public void Execute(Product item)
        {
            cart.AddToCart(item);
        }

        public void Undo(Product item)
        {
            cart.RemoveFromCart(item);
        }
    }

    class RemoveFromCartCommand : ICommand<Product>
    {
        private readonly ShoppingCart cart;
        private readonly Product product;

        public RemoveFromCartCommand(ShoppingCart cart, Product product)
        {
            this.cart = cart;
            this.product = product;
        }

        public void Execute(Product item)
        {
            cart.RemoveFromCart(item);
        }

        public void Undo(Product item)
        {
            cart.AddToCart(item);
        }
    }

    class CommandInvoker<T>
    {
        private readonly List<ICommand<T>> commandHistory = new List<ICommand<T>>();

        public void ExecuteCommand(ICommand<T> command, T item)
        {
            command.Execute(item);
            commandHistory.Add(command);
        }

        public void UndoLastCommands(int count, T item)
        {
            int actionsToUndo = Math.Min(count, commandHistory.Count);

            for (int i = commandHistory.Count - 1; i >= commandHistory.Count - actionsToUndo; i--)
            {
                commandHistory[i].Undo(item);
                Console.WriteLine($"Undid last command: {commandHistory[i].GetType().Name}");
            }
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            ShoppingCart cart = new ShoppingCart();
            CommandInvoker<Product> invoker = new CommandInvoker<Product>();

            Product product1 = new Product { Name = "Product 1", Manufacturer = "Manufacturer 1", Price = 19.99m };
            Product product2 = new Product { Name = "Product 2", Manufacturer = "Manufacturer 2", Price = 29.99m };

            ICommand<Product> addToCartCommand1 = new AddToCartCommand(cart, product1);
            ICommand<Product> addToCartCommand2 = new AddToCartCommand(cart, product2);

            invoker.ExecuteCommand(addToCartCommand1, product1);
            invoker.ExecuteCommand(addToCartCommand2, product2);

            cart.DisplayCart();

            invoker.UndoLastCommands(2, product2);
            cart.DisplayCart();


        }
    }
}
